FROM golang:1.13.5-alpine3.10

RUN apk add --update --no-cache git docker 
RUN wget -O /usr/local/bin/operator-sdk https://github.com/operator-framework/operator-sdk/releases/download/v0.12.0/operator-sdk-v0.12.0-x86_64-linux-gnu
RUN chmod +x /usr/local/bin/operator-sdk

ENV DOCKER_HOST=tcp://docker:2375
